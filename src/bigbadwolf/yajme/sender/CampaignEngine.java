package bigbadwolf.yajme.sender;

import bigbadwolf.yajme.base.Options;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author alessio 
 * 
 * Engine to create the common data of the email to send to
 * every recipient
 * 
 */
public class CampaignEngine implements Runnable, Comparable<CampaignEngine> {

    public static final int LOW = 75;
    public static final int NORMAL = 50;
    public static final int HIGH = 70;
    public static final int TEST = 100;
    public static final int EMAIL = 1;
    public static final int SMS = 0;
    public static final int INTERNAL = 0;
    public static final int EXTERNAL = 1;
    /**
     * The ID of the communication
     */
    private String comId;
    /**
     * communication email
     */
    private Email email;
    /**
     * pool of sender thread
     */
    private BlockingQueue<Runnable> mailerPool;
    /**
     * executor of thread
     */
//    private ExecutorService mailSender_old;
    private ThreadPoolExecutor mailSender;
    private boolean running = true;
    private boolean completed = false;
    private int priority = NORMAL;
    private String url;
    static Logger logger = Logger.getLogger(CampaignEngine.class);

    public CampaignEngine(String id, String url) {
        PropertyConfigurator.configure("log4j.properties");
        try {
            comId = id;
            this.url = url;
            mailerPool = new ArrayBlockingQueue<>(Integer.parseInt(Options.getOption("MAX_MAILS")), true);
            mailSender = new ThreadPoolExecutor(
                    Integer.parseInt(Options.getOption("MAX_SENDERS")),
                    Integer.parseInt(Options.getOption("MAX_SENDERS")),
                    60, TimeUnit.SECONDS,
                    new ArrayBlockingQueue<>(Integer.parseInt(Options.getOption("MAX_SENDERS")), true));
            mailSender.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
            MailXMLParser parser = new MailXMLParser();
            email = parser.parseXML(comId, this.url);
            email.CampaignID = comId;
            email.addAttachmentsToMail();
        } catch (NumberFormatException ex) {
            logger.fatal("Error trying to create campaign from " + url + ": " + id + " Error: " + ex.getLocalizedMessage());
        }
    }

    public void pauseComm() {
        running = false;
    }

    public void resumeComm() {
        running = true;
    }

    public boolean isCompleted() {
        return completed;
    }

    private void startSending() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                Thread.currentThread().setName("Put thread in pool " + comId);
                while (!completed) {
                    try {
                        while (mailerPool.size() > 0) {
                            Runnable thread = mailerPool.take();
                            if (thread != null) {
                                mailSender.execute(thread);
                            }
                        }
                    } catch (InterruptedException ex) {
                        logger.fatal("can't take another mail to send. Error: " + ex.getLocalizedMessage());
                    }
                }
                mailSender.shutdown();
                try {
                    mailSender.awaitTermination(5, TimeUnit.MINUTES);
                } catch (InterruptedException ex) {
                    logger.fatal("can't wait termination of queue: " + ex.getLocalizedMessage());
                }
            }
        }).start();

    }

    private void addMail() {
        startSending();
        while (!completed) {
            try {
                String data = ServerRequest.getRecipient(this.comId, url, Options.getOption("MAX_MAILS"));
                if (!data.isEmpty() || data.length() > 3) {
                    logger.info("Received recipients from " + url);
                    logger.info("recipients: " + data);
                    ObjectMapper mapper = new ObjectMapper();
                    Map<String, Object> recpts = mapper.readValue(data, Map.class);
                    String[] ids = recpts.keySet().toArray(new String[0]);
                    if (ids.length > 0) {
                        for (String id : ids) {
                            try {
                                Map<String, String> customization = (Map<String, String>) recpts.get(id);
                                switch (Integer.parseInt(Options.getOption("externalSMTP"))) {
                                    case INTERNAL:
                                        mailerPool.put(new MailSender(email, id, customization, url));
                                        break;
                                    case EXTERNAL:
                                        mailerPool.put(new SMTPSender(email, id, customization, url));
                                        break;
                                }
                            } catch (InterruptedException | NumberFormatException ex) {
                                logger.error("Error trying to create sender thread. Error: " + ex.getLocalizedMessage());
                            }
                        }
                    }

                } else {
                    logger.info(comId + ": No more recipients");
                    completed = true;
                }
            } catch (IOException ex) {
                logger.error("Error trying to get recipient. Error: " + ex.getLocalizedMessage());
            }
        }
    }

    @Override
    public void run() {
        Thread.currentThread().setName("Campaign " + comId);
        addMail();
    }

    @Override
    public int compareTo(CampaignEngine o) {
        Integer p = o.priority;
        return p.compareTo(this.priority);
    }
}
