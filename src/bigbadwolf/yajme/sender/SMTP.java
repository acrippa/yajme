package bigbadwolf.yajme.sender;

import bigbadwolf.yajme.base.Options;
import java.util.Properties;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;

/**
 *
 * @author alessio
 */
public class SMTP {

    public static Session connect() {
        Properties props = System.getProperties();
        String timeout = Options.getOption("smtpTimeout");
        props.put("mail.smtp.host", Options.getOption("smtpHost"));
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", Options.getOption("smtpPort"));
        props.put("mail.smtp.from", Options.getOption("smtpBounce"));
        //props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.connectiontimeout", timeout);
        props.put("mail.smtp.timeout", timeout);
        props.put("mail.smtp.writetimeout", timeout);
        SMTPAuthenticator auth = new SMTPAuthenticator();
        return Session.getInstance(props, auth);
    }

    private static class SMTPAuthenticator extends javax.mail.Authenticator {

        @Override
        public PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(Options.getOption("smtpAccount"), Options.getOption("smtpPassword"));
        }
    }
}
