package bigbadwolf.yajme.sender;

import com.sun.mail.smtp.SMTPMessage;
import bigbadwolf.yajme.base.Options;
import java.io.IOException;
import java.util.*;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author alessio 
 * 
 * Thread to send a single email to the recipient with the
 * custom data
 * 
 */
public class SMTPSender extends Thread {

    /**
     * The email object to send to recipients
     */
    private Email email;
    /**
     * Tell the thread to finish if set to true from outside NOTE: never used
     */
    public boolean mustExit;
    /**
     * recipient
     */
    private String recipient;
    private ArrayList<String> ccs;
    private Map<String, String> customization;
    private String id;
    private String url;
    static Logger logger = Logger.getLogger(SMTPSender.class);

    private String trim(String str, char ch) {
        if (str == null) {
            return null;
        }
        int count = str.length();
        int len = str.length();
        int st = 0;
        int off = 0;
        char[] val = str.toCharArray();

        while ((st < len) && (val[off + st] <= ch)) {
            st++;
        }
        while ((st < len) && (val[off + len - 1] <= ch)) {
            len--;
        }
        return ((st > 0) || (len < count)) ? str.substring(st, len) : str;
    }

    /**
     *
     * @param email
     * @param id
     * @param custom
     * @param url
     */
    public SMTPSender(Email email, String id, Map<String, String> custom, String url) {
        try {
        PropertyConfigurator.configure("log4j.properties");
        this.email = email;
        String to = custom.get("recipient");
        custom.remove("recipient");
        ccs = new ArrayList<>();
        to = to.trim();
        to = to.substring(0, to.length() - 1);
        
        String recipientName = to.split("<")[0];
        to = to.split("<")[1];
        to = to.trim();
        to = trim(to, ';');
        String[] tos = to.split(";");
        
        recipient = recipientName + " <" + tos[0].trim() + ">";
        logger.info("Recipient found: " + recipient);
        for (int i = 1; i < tos.length; i++) {
            String cc = tos[i].trim();
            if (cc.length() > 2) {
                ccs.add(recipientName + " <" + cc + ">");
                logger.info("CC found: " + cc);
            }
        }
        customization = custom;
        this.id = id;
        this.url = url;
        } catch (Exception ex) {
                logger.error("Error: " + ex.getLocalizedMessage());
                ex.getMessage();
            }
    }

    /**
     * The internal thread loop for sending emails
     */
    @Override
    public void run() {
        Thread.currentThread().setName("Sending Mail: " + id);
        String idMail = id + "." + email.senderID;
        ServerRequest.SetID(id, idMail, url);
        send(idMail);
    }

    private void send(String idMail) {
        try {
            SMTPMessage msg = email.generateSMTPMessage(recipient, ccs, customization, idMail);
            int tries = 0;
            boolean sent = false;            
            while (tries < Integer.parseInt(Options.getOption("MAX_TRIES")) && !sent) {
                try {
                    Session session;
                    Transport bus;
                    session = SMTP.connect();
                    logger.info("Connecting to smtp server");
                    long startTime = System.currentTimeMillis();
                    bus = session.getTransport("smtp");
                    bus.connect();
                    logger.info("Sending message");
                    bus.sendMessage(msg, msg.getAllRecipients());
                    bus.close();
                    long stopTime = System.currentTimeMillis();
                    long elapsedTime = stopTime - startTime;
                    logger.info("Message sent, time: " + elapsedTime);
                    ServerRequest.SetStatus(id, ServerRequest.SENT, url);
                    sent = true;
                } catch (javax.mail.MessagingException ex) {
                    logger.error("Error sending email: " + ex.getLocalizedMessage());
                    tries++;
                }
            }

            if (!sent) {
                logger.error("To many tries sending to: " + recipient);
                ServerRequest.SetStatus(id, ServerRequest.RETRIES, url);
            }

        } catch (javax.mail.internet.AddressException ex) {
            logger.error("Error converting email address " + recipient + ": " + ex.getLocalizedMessage());
            ServerRequest.SetStatus(id, ServerRequest.ADDRESS_ERROR, url);
        } catch (NumberFormatException | MessagingException ex) {
            logger.error("Error sending email: " + ex.getLocalizedMessage());
            ServerRequest.SetStatus(id, ServerRequest.SEND_ERROR, url);
        }
    }
}
