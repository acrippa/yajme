package bigbadwolf.yajme.sender;

import com.sun.mail.smtp.SMTPMessage;
import bigbadwolf.yajme.base.Options;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.Message;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.xbill.DNS.Lookup;
import org.xbill.DNS.MXRecord;
import org.xbill.DNS.Record;
import org.xbill.DNS.Type;
import de.agitos.dkim.DKIMSigner;
import de.agitos.dkim.SMTPDKIMMessage;
import java.io.ByteArrayInputStream;
import org.xbill.DNS.TextParseException;

/**
 *
 * @author alessio
 */
public class MailSender extends Thread {

    /**
     * The email object to send to recipients
     */
    private Email email;
    /**
     * recipient
     */
    private String recipient;
    private ArrayList<String> ccs;
    private Map<String, String> customization;
    private String id;
    private String url;
    static Logger logger = Logger.getLogger(MailSender.class);

    private String trim(String str, char ch) {
        if (str == null) {
            return null;
        }
        int count = str.length();
        int len = str.length();
        int st = 0;
        int off = 0;
        char[] val = str.toCharArray();

        while ((st < len) && (val[off + st] <= ch)) {
            st++;
        }
        while ((st < len) && (val[off + len - 1] <= ch)) {
            len--;
        }
        return ((st > 0) || (len < count)) ? str.substring(st, len) : str;
    }

    public MailSender(Email email, String id, Map<String, String> custom, String url) {
        PropertyConfigurator.configure("log4j.properties");
        this.email = email;
        String to = custom.get("recipient");
        custom.remove("recipient");
        ccs = new ArrayList<>();
        to = to.trim();
        to = to.substring(0, to.length() - 1);
        String destName = to.split("<")[0];
        to = to.split("<")[1];
        to = to.trim();
        to = trim(to, ';');
        String[] tos = to.split(";");
        recipient = destName + " <" + tos[0].trim() + ">";
        logger.info("Recipient found: " + recipient);
        for (int i = 1; i < tos.length; i++) {
            String cc = tos[i].trim();
            if (cc.length() > 2) {
                ccs.add(destName + " <" + cc + ">");
                logger.info("CC found: " + cc);
            }
        }
        customization = custom;
        this.id = id;
        this.url = url;
    }

    /**
     * The internal thread loop for sending emails
     */
    @Override
    public void run() {
        Thread.currentThread().setName("Sending Mail: " + id);
        String idMail = id + "." + email.senderID;
        ServerRequest.SetID(id, idMail, url);
        logger.info(recipient + ": Sending mail " + id);
        genMail(idMail);
    }

    private void genMail(String idMail) {
        try {
            SMTPMessage msg = email.generateSMTPMessage(recipient, ccs, customization, idMail);
            Message signed_msg = signMessage(msg);
            String status = sendMail(signed_msg,
                    (InternetAddress)msg.getRecipients(Message.RecipientType.TO)[0],
                    (InternetAddress [])msg.getRecipients(Message.RecipientType.CC));
            ServerRequest.SetStatus(id, status, url);
        } catch (javax.mail.internet.AddressException ex) {
            logger.error("Error converting email address " + recipient + ": " + ex.getLocalizedMessage());
            ServerRequest.SetStatus(id, ServerRequest.ADDRESS_ERROR, url);
        } catch (MessagingException ex) {
            logger.error("Error creating email: " + ex.getLocalizedMessage());
            ServerRequest.SetStatus(id, ServerRequest.SEND_ERROR, url);
        }
    }

    private Message signMessage(MimeMessage msg) {
        // get a JavaMail Session object 
        Session session = Session.getDefaultInstance(System.getProperties());
        try {
            // get DKIMSigner object
            DKIMSigner dkimSigner = new DKIMSigner(
                    Options.getOption("mail.smtp.dkim.signingdomain"),
                    Options.getOption("mail.smtp.dkim.selector"),
                    Options.getOption("mail.smtp.dkim.privatekey"));

            // set an address of the user on behalf this message was signed;
            dkimSigner.setIdentity(
                    Options.getOption("mail.smtp.dkim.signingidentity") +"@" + 
                            Options.getOption("mail.smtp.dkim.signingdomain"));

            // construct the JavaMail message using the DKIM message type from DKIM for JavaMail
            return new SMTPDKIMMessage(
                    session, 
                    new ByteArrayInputStream(msg.toString().getBytes()), 
                    dkimSigner);
        } catch (Exception ex) {
            return msg;
        }
    }

    public String sendMail(Message msg, InternetAddress to, InternetAddress[] ccs) {
        String status = send(msg, to);
        for (InternetAddress cc : ccs) {
            send(msg, cc);
        }
        return status;
    }

    private String send(Message msg, InternetAddress recipient) {
        String status = "";
        try {
            String domain = recipient.getAddress().split("@")[1];
            Record[] records = new Lookup(domain, Type.MX).run();
            if (null != records) {
                Arrays.sort(records, (Record r1, Record r2) -> (((MXRecord) r1).getPriority() - ((MXRecord) r2).getPriority()));
                int tries = 0;
                boolean sent = false;
                if (records.length >= 1) {
                    while (tries < Integer.parseInt(Options.getOption("MAX_TRIALS")) && !sent) {
                        for (Record mx : records) {
                            try {
                                String server = ((MXRecord) mx).getTarget().toString();
                                logger.info(recipient + ": trying mx server: " + server);
                                Properties props = System.getProperties();
                                props.put("mail.smtp.host", server);
                                props.put("mail.smtp.localhost", Options.getOption("hostname"));
                                props.put("mail.smtp.connectiontimeout", Options.getOption("smtpTimeout"));
                                props.put("mail.smtp.timeout", Options.getOption("smtpTimeout"));
                                props.put("mail.smtp.sendpartial", "true");
                                props.put("mail.smtp.from", email.getBounce());
                                Transport bus = Session.getInstance(props).getTransport("smtp");
                                bus.connect();
                                Address[] addr = new Address[1];
                                addr[0] = recipient;
                                bus.sendMessage(msg, addr);
                                bus.close();
                                status = ServerRequest.SENT;
                                sent = true;
                                logger.info(recipient + ": mail sent");
                                break;
                            } catch (SendFailedException ex) {
                                if (ex.getMessage().equals("Invalid Address") || ex.getMessage().equals("Invalid Addresses")) {
                                    status = ServerRequest.ADDRESS_ERROR;
                                    tries++;
                                    logger.info(recipient + ": Invalid Address");
                                } else {
                                    status = ServerRequest.SEND_ERROR;
                                    tries++;
                                    logger.info(recipient + "error: " + ex.getLocalizedMessage());
                                }
                            } catch (MessagingException ex) {
                                if (ex.getNextException().getMessage().equals("Read timed out") || ex.getMessage().equals("Exception reading response")) {
                                    logger.error(recipient + " error: " + ex.getLocalizedMessage());
                                    status = ServerRequest.SEND_ERROR;
                                    tries++;
                                } else if (ex.getMessage().equals("Invalid Address") || ex.getMessage().equals("Invalid Addresses")) {
                                    status = ServerRequest.ADDRESS_ERROR;
                                    tries++;
                                    logger.info(recipient + ": Invalid Address");
                                } else {
                                    logger.error(recipient + " error: " + ex.getLocalizedMessage());
                                    status = ServerRequest.SEND_ERROR;
                                    tries++;
                                }
                            }
                        }
                    }
                    if (tries >= Integer.parseInt(Options.getOption("MAX_TRIALS"))) {
                        if (!status.equals(ServerRequest.ADDRESS_ERROR)) {
                            status = ServerRequest.RETRIES;
                        }
                    }
                } else {
                    status = ServerRequest.SEND_ERROR;
                }
            } else {
                logger.error(recipient + " no MX server found");
                status = ServerRequest.SEND_ERROR;
            }
        } catch (NumberFormatException | TextParseException ex) {
            logger.error(recipient + " error: " + ex.getLocalizedMessage());
            status = ServerRequest.SEND_ERROR;
        }
        return status;
    }
}
