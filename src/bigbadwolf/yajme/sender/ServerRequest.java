package bigbadwolf.yajme.sender;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 *
 * @author alessio
 */
public class ServerRequest {

    public static String WORKING = "200";
    public static String ADDRESS_ERROR = "300";
    public static String RETRIES = "400";
    public static String SEND_ERROR = "500";
    public static String SENT = "600";
    
    public static String getCampaigns(String url) {
        try {
            // API_URL/campaigns
            return request(url + "/campaigns");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return "";
        }
    }

    public static String getRecipient(String campaignId, String url, String max) {
        try {
            // API_URL/recipients/{CampaignID}/{MaxNumberOfRecipients}
            return request(url + "/recipients/"+ campaignId + "/" + max);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return "";
        }
    }

    public static String SetID(String id, String mailId, String url) {
        try {
            // API_URL/email/{EmailID}/{ID}
            return request(url + "/email/"+ id + "/" + mailId);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return ex.getMessage();
        }
    }

    public static String SetStatus(String id, String status, String url) {
        try {
            // API_URL/email/{EmailID}/{status}
            return request(url + "/email/"+ id + "/" + status);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return ex.getMessage();
        }
    }
     
    private static String request(String url) throws Exception {
        URL api = new URL(url);
        URLConnection ac = api.openConnection();
        ac.setDoOutput(true);
        BufferedReader rd;
        String response = "";
        rd = new BufferedReader(new InputStreamReader(ac.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            response += line;
        }
        rd.close();
        return response;
    }
}
