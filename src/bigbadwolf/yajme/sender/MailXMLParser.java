package bigbadwolf.yajme.sender;

import bigbadwolf.yajme.base.DownloadFile;
import java.net.*;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import bigbadwolf.yajme.base.Options;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author alessio
 * 
 * create the mail from a XML input stream
 */
public class MailXMLParser extends DefaultHandler {

    String tmp = "";
    String apiUrl;
    String ID;
    Email mail = new Email();
    static Logger logger = Logger.getLogger(MailXMLParser.class);

    @Override
    public void startElement(String uri, String localName, String qName,
            Attributes attributes) throws SAXException {
        tmp = "";
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        tmp = new String(ch, start, length);
    }

    @Override
    public void endElement(String uri, String localName,
            String qName) throws SAXException {

        if (qName.equalsIgnoreCase("from")) {
            mail.setFrom(tmp);
        }
        if (qName.equalsIgnoreCase("bounce")) {
            mail.setBounce(tmp);
        }
        if (qName.equalsIgnoreCase("replyTo")) {
            mail.setReplyTo(tmp);
        }
        if (qName.equalsIgnoreCase("deliveryReceipt")) {
            mail.setReceipt(Boolean.valueOf(tmp));
        }
        if (qName.equalsIgnoreCase("subject")) {
            mail.setSubject(tmp);
        }
        if (qName.equalsIgnoreCase("body")) {
            mail.setBody(tmp);
        }
        if (qName.equalsIgnoreCase("attachment")) {
            if (!tmp.isEmpty()) {
                int i = 1;
                for (String x : tmp.split(";")) {
                    mail.addAttachment("Attachment " + i++, x);
                    try {
                        DownloadFile.download(apiUrl, ID, x, Options.getOption("attachmentPath"));
                    } catch (Exception ex) {
                        logger.error("Can't download file " + x + " " + ex.getMessage());
                    }
                }
            }
        }
    }

    public Email parseXML(String campaignId, String requestURL) {
        try {
            PropertyConfigurator.configure("log4j.properties");
            apiUrl = requestURL;
            ID = campaignId;
            SAXParserFactory factory = SAXParserFactory.newInstance();
            URL url = new URL(requestURL + "/campaign/" + campaignId + "/email");
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            InputSource is = new InputSource(conn.getInputStream());
            is.setEncoding("UTF-8");
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse(is, this);
        } catch (IOException | ParserConfigurationException | SAXException ex) {
            logger.error(ex.getMessage());
        }
        return mail;
    }
}
