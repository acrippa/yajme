package bigbadwolf.yajme.sender;

/**
 *
 * @author alessio
 */
import bigbadwolf.yajme.base.Options;
import com.sun.mail.smtp.SMTPMessage;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * Store the common data of the email to send
 */
public class Email {

    public static final String EMAIL_REGEX = "\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
    public static final String IMG_SRC_REGEX = "<img.*?src[\\s+]{0,}\\=[\\s+]{0,}\\\"(.*?)\\\"";
    public String CampaignID;
    private String from;
    private String bounce;
    private String replyTo;
    private boolean deliveryReceipt = true;
    private String subject;
    private String htmlMessage;
    public TreeMap<String, String> attachs;
    private ArrayList<MimeBodyPart> attachments;

    public String senderID;
    static Logger logger = Logger.getLogger(Email.class);

    Email() {
        PropertyConfigurator.configure("log4j.properties");
        attachs = new TreeMap<>();
    }

    public void setFrom(String sndr) {
        from = sndr;
    }

    public void setReplyTo(String rply) {
        replyTo = rply;
    }

    public void setReceipt(boolean rcpt) {
        deliveryReceipt = rcpt;
    }

    public void setBounce(String bounce) {
        this.bounce = bounce;
    }

    public void setBody(String body) {
        htmlMessage = body;
    }

    public void setSubject(String sbj) {
        subject = sbj;
    }

    public void addAttachment(String key, String value) {
        attachs.put(key, value);
    }

    public String getFrom() {
        return from;
    }

    public String getBounce() {
        return bounce;
    }

    public String getReplyTo() {
        return replyTo;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return htmlMessage;
    }

    public boolean getReceipt() {
        return deliveryReceipt;
    }

    public void addAttachmentsToMail() {
        attachments = new ArrayList<>();
        try {
            if (attachs != null) {
                if (attachs.size() > 0) {
                    for (Entry<String, String> file : attachs.entrySet()) {
                        if (file.getValue().length() > 0) {
                            MimeBodyPart attachBodyPart = new MimeBodyPart();
                            File mimeFile = new File(Options.getOption("attachmentPath") + CampaignID + "/" + file.getValue());
                            DataSource source = new FileDataSource(mimeFile);
                            attachBodyPart.setDataHandler(new DataHandler(source));
                            attachBodyPart.setDisposition(Part.ATTACHMENT);
                            attachBodyPart.setFileName("" + file.getKey() + file.getValue().substring(file.getValue().lastIndexOf(".")) + "");
                            attachments.add(attachBodyPart);
                        }
                    }
                }
            }
        } catch (MessagingException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private MimeMultipart generateMimePart(Map<String, String> customization) {
        MimeMultipart mp_big = new MimeMultipart();

        String body = htmlMessage;
        try {
            for (String key : customization.keySet().toArray(new String[0])) {
                body = body.replaceAll(key, customization.get(key));
            }
            mp_big.setSubType("mixed");
            MimeMultipart multipart = new MimeMultipart();
            multipart.setSubType("alternative");
            MimeMultipart mp_mixed = new MimeMultipart();
            mp_mixed.setSubType("related");

            //mp_big.
            BodyPart messageBodyPart = new MimeBodyPart();
            BodyPart messageBodyPart_text = new MimeBodyPart();
            BodyPart messageBodyPart_html = new MimeBodyPart();
            BodyPart SubmessageBodyPart_html = new MimeBodyPart();
            // Email Body in text/plain
            messageBodyPart_text.setText("Content in HTML.");
            // --

            SubmessageBodyPart_html.setContent(body.replaceAll("\\\\'", "'"), "text/html;charset=\"utf-8\"");
            mp_mixed.addBodyPart(SubmessageBodyPart_html);

            messageBodyPart.setContent(multipart);
            messageBodyPart_html.setContent(mp_mixed);
            multipart.addBodyPart(messageBodyPart_text);
            multipart.addBodyPart(messageBodyPart_html);

            messageBodyPart.setContent(multipart);
            mp_big.addBodyPart(messageBodyPart);
            if (attachments.size() > 0) {
                for (MimeBodyPart m : attachments) {
                    mp_big.addBodyPart(m);
                }
            }
        } catch (MessagingException ex) {
            System.out.println(ex.getMessage());
        }
        return mp_big;
    }

    public SMTPMessage generateSMTPMessage(String recipient,
            ArrayList<String> ccs,
            Map<String, String> customization,
            String idMail) throws MessagingException {
        SMTPMessage msg = new SMTPMessage(Session.getDefaultInstance(System.getProperties()));
        InternetAddress addressTo = new InternetAddress(recipient);
        InternetAddress[] addressCC = new InternetAddress[ccs.size()];
        int i = 0;
        for (String cc : ccs) {
            try {
                InternetAddress tmp = new InternetAddress(cc);
                addressCC[i] = tmp;
                i++;
            } catch (javax.mail.internet.AddressException ex) {
                logger.error("Error cc: " + ex.getLocalizedMessage());
            }
        }
        String subj = this.subject;
        for (String key : customization.keySet().toArray(new String[0])) {
            subj = subj.replaceAll(key, customization.get(key));
        }
        msg.setReturnOption(SMTPMessage.RETURN_HDRS);
        msg.setNotifyOptions(SMTPMessage.NOTIFY_SUCCESS | SMTPMessage.NOTIFY_FAILURE);
        msg.setRecipient(Message.RecipientType.TO, addressTo);
        if(i>0){
            msg.setRecipients(Message.RecipientType.CC, addressCC);
        }
        msg.setSubject(subj);
        msg.addHeader("From", from);
        msg.addHeader("Reply-To", replyTo);
        msg.addHeader("Return-Path", bounce);
        msg.addHeader("Errors-To", bounce);
        msg.setSentDate(new Date());
        msg.setContent(generateMimePart(customization));
        msg.setHeader("Message-ID", "<" + idMail + "@" + Options.getOption("hostname") + ">");
        msg.setEnvelopeFrom(bounce);
        msg.saveChanges();
        return msg;
    }

}
