package bigbadwolf.yajme.base;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

/**
 *
 * @author alessio
 */
public class DownloadFile {

    public static void download(String site, String campaignId, String file, String path) throws Exception {
        URL url = new URL(site + "/campaign/" + campaignId + "/file/" + file);
        new File(path + campaignId + "/").mkdir();
        if (file.contains("/")) {
            new File(path + campaignId + "/" + file.substring(0, file.lastIndexOf("/"))).mkdir();
        }
        ReadableByteChannel rbc = Channels.newChannel(url.openStream());
        try (FileOutputStream fos = new FileOutputStream(path + campaignId + "/" + file)) {
            fos.getChannel().transferFrom(rbc, 0, 1 << 24);
        }
    }
}