package bigbadwolf.yajme.base;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author alessio
 */
public class Main {
    
    static final Logger LOGGER = Logger.getLogger(Main.class);
    
    public static void main(String[] args) {
        try {
            PropertyConfigurator.configure("log4j.properties");
            LOGGER.info("Start thread to retrieve communcations");
            new Thread(new YAJME()).start();
        } catch (Exception ex) {
            LOGGER.fatal("ALERT MAIN: " + ex.getLocalizedMessage());
        }
    }
}