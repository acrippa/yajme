package bigbadwolf.yajme.base;

import bigbadwolf.yajme.sender.CampaignEngine;
import bigbadwolf.yajme.sender.ServerRequest;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author alessio
 */
public class YAJME implements Runnable {

    static final Logger LOGGER = Logger.getLogger(Main.class);

    @Override
    public void run() {
        try {
            while (true) {
                PropertyConfigurator.configure("log4j.properties");
                Thread.currentThread().setName("Retrieving campaigns");
                int sleep = Integer.parseInt(Options.getOption("SLEEPTIME"));
                HashMap<String, CampaignEngine> campaigns = new HashMap<>();
                String apiURL = Options.getOption("api");
                ArrayList<String> ids = new ArrayList<>();
                try {
                    while (true) {
                        try {
                            LOGGER.info("Retrieving campaigns from " + apiURL);
                            String response = ServerRequest.getCampaigns(apiURL);
                            if (!response.isEmpty()) {
                                String[] tmps = response.split(":");
                                for (String id : tmps) {
                                    if (!id.isEmpty() && !campaigns.containsKey(id)) {
                                        LOGGER.info("Add campaign ID: " + id);
                                        CampaignEngine engine = new CampaignEngine(id, apiURL);
                                        new Thread(engine).start();
                                        campaigns.put(id, engine);
                                        ids.add(id);
                                    }
                                }
                            }
                            for (String commId : ids) {
                                if (campaigns.get(commId).isCompleted()) {
                                    LOGGER.info("Remove campaign: " + commId);
                                    campaigns.remove(commId);
                                    ids.remove(commId);
                                }
                            }
                            Thread.sleep(sleep);
                        } catch (InterruptedException | NumberFormatException ex) {
                            LOGGER.error(ex.getMessage());
                        }
                    }
                } catch (Exception ex) {
                    LOGGER.fatal(ex.getMessage());
                }
                LOGGER.error("ALERT! Main loop terminated. Relaunching...");
            }
        } catch (NumberFormatException ex) {
            LOGGER.fatal(ex.getMessage());
        }
    }
}
