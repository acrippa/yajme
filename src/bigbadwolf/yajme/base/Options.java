package bigbadwolf.yajme.base;

import java.util.ResourceBundle;

/**
 *
 * @author alessio
 */
public class Options {

    private static final String BUNDLE_NAME = "bigbadwolf.yajme.base.yajme";
    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

    public static String getOption(String key) {
        return RESOURCE_BUNDLE.getString(key);
    }
}